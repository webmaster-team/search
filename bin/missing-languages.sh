#!/bin/sh

# Find languages for which there are, apparently, translations
# but that we are not listing in the 'languages' file

for lang in $(find /srv/www.debian.org/www/ -maxdepth 1 -name '*.??*.html' |
		sed 's,.*/,,' | cut -d. -f2 | sort -u); do
    grep -q $lang languages || echo not found: $lang
done
