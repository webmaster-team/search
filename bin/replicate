#!/bin/sh

####################
#    Copyright (C) 2011 by Raphael Geissert <geissert@debian.org>
#
#
#    This file is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This file is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this file.  If not, see <http://www.gnu.org/licenses/>.
####################


set -eu

umask 0002

basedir=/srv/search.debian.org
wwwdir=/srv/www.debian.org/www/
url=https://www.debian.org/
dbdir=$basedir/xapian/data/
logfile=$basedir/log/cronjobs.log
bindir=$basedir/bin
uip_limit=6
uip_delay=$((5*60))

PATH="$bindir:$PATH"
export PATH

[ -d "$basedir/log" ] || mkdir -p "$basedir/log"
savelog -c 62 -m 664 -t $logfile >/dev/null 2>&1

rc=0
cd "$dbdir"
while read code lang stemming stemming_lang unused; do
    # Make English the default
    case "$code" in
	en)
	    if ! [ -L "$dbdir/$code" ]; then
		ln -s default "$dbdir/$code"
	    fi
	    code=default
	;;
    esac

    # Wait for a free ssh tunnel slot.
    while [ `ps -ef|grep -c '^portfor.*\<7010\>'` -ge 10 ] ; do echo "$code: waiting for ssh tunnel slot" ; sleep 60 ; done
    xapian-replicate -h localhost -p 7010 "$code" -v -o || { rc=$?; echo "$code: failed with exit code $rc"; }
done < $basedir/languages >> $logfile 2>&1

[ $rc -eq 0 ] || exit $rc

# So DSA monitoring can compare time of most recent website update with time of
# most recent successful replication.
date > /srv/search.debian.org/timestamp
