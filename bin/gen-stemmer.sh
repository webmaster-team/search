#!/bin/sh

set -e

# Usage: bin/gen-stemmer.sh > xapian/templates/inc/stemmer
# TODO: the above file and this script shouldn't really exist

# Default to none if we don't have a stemmer for the language - otherwise
# the default is to use the English stemmer (#905126)
echo '$set{stemmer,none}'
echo '$if{$eq{$dbname,default},$set{stemmer,en}}'
while read code lang stemming unused; do
    if [ stemming = "$stemming" ]; then
	shortcode="${code%-??}"
	echo '$if{$eq{$dbname,'"$code"'},$set{stemmer,'"$shortcode"'}}'
    fi
done < languages
