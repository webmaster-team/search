#!/bin/sh

set -e

# Usage: bin/gen-languages.sh > xapian/templates/inc/languages
# TODO: the above file and this script shouldn't really exist

while read code lang unused; do

    selected='$if{$eq{$dbname,'"$code"'},selected="selected"}'
    if [ en = "$code" ]; then
	selected='$if{$or{$eq{$dbname,'"$code"'},$eq{$dbname,default}},selected="selected"}'
    fi

    printf '<option %s value="%s">%s</option>\n' \
	"$selected" "$code" "$lang"
done < languages
